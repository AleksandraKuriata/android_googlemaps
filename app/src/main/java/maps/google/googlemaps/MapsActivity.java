package maps.google.googlemaps;

import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnPolylineClickListener {

    private GoogleMap mMap;
    private Town activeTown;
    private Route activeRoute;
    private ArrayList<Route> routes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        routes=new ArrayList<>();
        Spinner spinner = (Spinner) findViewById(R.id.towns);
        final ArrayList<Town> townList =new ArrayList<>();
        InputStream is = getResources().openRawResource(R.raw.miasta);
        Writer writer = new StringWriter();
        char[] buffer = new char[1024];
        try {
            Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            int n;
            while ((n = reader.read(buffer)) != -1) {
                writer.write(buffer, 0, n);
            }
            is.close();
            String jsonString = writer.toString();
            JSONObject jsonObject = new JSONObject(jsonString);
            JSONArray towns = jsonObject.getJSONArray("towns");
            for(int i=0;i<towns.length();i++){
                JSONObject temp = towns.getJSONObject(i);
                townList.add(new Town(temp.getString("name"),temp.getJSONArray("coordinates").getInt(0),temp.getJSONArray("coordinates").getInt(1)));
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
        MyAdapter myTownAdapter = new MyAdapter(this,townList);
        spinner.setAdapter(myTownAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                LatLng latLng = new LatLng(townList.get(position).latitude,townList.get(position).longitude);
                //mMap.addMarker(new MarkerOptions().position(latLng).title(townList.get(position).name));
                activeTown=townList.get(position);
                mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnPolylineClickListener(this);
        // Add a marker in Sydney and move the camera
        //LatLng sydney = new LatLng(-34, 151);
        //mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        //mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
    }

    @Override
    public void onPolylineClick(Polyline polyline) {
        if(activeRoute!=null){
            for (Marker marker : activeRoute.markerArrayList) {
                marker.setVisible(false);
            }
        }
        for (Route route : routes) {
            if(polyline.equals(route.polyline)){
                activeRoute=route;
                for (Marker marker : activeRoute.markerArrayList) {
                    marker.setVisible(true);
                }
                break;
            }
        }
    }

    public void add(View view){
        if(activeRoute!=null){
            List<LatLng> latLngList = activeRoute.polyline.getPoints();
            latLngList.add(new LatLng(activeTown.latitude,activeTown.longitude));
            Marker marker = mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(activeTown.latitude,activeTown.longitude))
                    .title(activeTown.name));
            activeRoute.polyline.setPoints(latLngList);
            activeRoute.markerArrayList.add(marker);
            int distance=0;
            for(int i = 1; i < activeRoute.markerArrayList.size(); i++){
                System.out.println("helo");
                Location loc1 = new Location("");
                loc1.setLatitude(activeRoute.markerArrayList.get(i-1).getPosition().latitude);
                loc1.setLongitude(activeRoute.markerArrayList.get(i-1).getPosition().longitude);
                Location loc2 = new Location("");
                loc2.setLatitude(activeRoute.markerArrayList.get(i).getPosition().latitude);
                loc2.setLongitude(activeRoute.markerArrayList.get(i).getPosition().longitude);
                distance += (int)loc1.distanceTo(loc2);
            }
            marker.setSnippet("Distance: " + distance/1000 +"km" );
            marker.showInfoWindow();
        }
    }
    public void make(View view){
        if(activeRoute!=null){
            for (Marker marker : activeRoute.markerArrayList) {
                marker.setVisible(false);
            }
        }
        Polyline line = mMap.addPolyline(new PolylineOptions()
                .add(new LatLng(activeTown.latitude,activeTown.longitude)).clickable(true));
        Marker marker = mMap.addMarker(new MarkerOptions()
                .position(new LatLng(activeTown.latitude,activeTown.longitude))
                .title(activeTown.name).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
        marker.showInfoWindow();
        activeRoute = new Route(line,marker);
        routes.add(activeRoute);
    }
    public void delete(View view){
        if(activeRoute!=null) {
            activeRoute.polyline.remove();
            for (Marker marker : activeRoute.markerArrayList) {
                marker.remove();
            }
            routes.remove(activeRoute);
            activeRoute = null;
        }
    }
}
