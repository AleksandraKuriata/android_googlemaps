package maps.google.googlemaps;

import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.Polyline;

import java.util.ArrayList;

public class Route {
    public Polyline polyline;
    public ArrayList<Marker> markerArrayList;
    public Route(Polyline polyline, Marker marker){
        this.polyline=polyline;
        markerArrayList=new ArrayList<>();
        markerArrayList.add(marker);
    }
}
