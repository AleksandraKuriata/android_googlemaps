package maps.google.googlemaps;

/**
 * Created by Ola on 28.05.2017.
 */

public class Town {
    public String name;
    public int latitude;
    public int longitude;
    public Town(String name, int latitude, int longitude){
        this.name=name;
        this.latitude=latitude;
        this.longitude=longitude;
    }
}
