package maps.google.googlemaps;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class MyAdapter extends ArrayAdapter<Town>{
    private final ArrayList<Town> towns;
    private final Activity context;
    public MyAdapter(Activity context, ArrayList<Town> towns) {
        super(context, R.layout.spinner_element, towns);
        this.context = context;
        this.towns= towns;
    }
    @Override
    public View getView(int position, View view, ViewGroup parent) {
        if(view == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            view = inflater.inflate(R.layout.spinner_element, parent, false);
        }
        TextView text = (TextView)view.findViewById(R.id.text);
        text.setText(towns.get(position).name);
        return view;
    }

    @Override
    public View getDropDownView(int position, @Nullable View view, @NonNull ViewGroup parent) {
        if(view == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            view = inflater.inflate(R.layout.spinner_element, parent, false);
        }
        TextView text = (TextView)view.findViewById(R.id.text);
        text.setText(towns.get(position).name);
        return view;
    }
}
